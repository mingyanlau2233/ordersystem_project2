# Restaurant Ordering System

## Front End

- [ ] Login page
- [ ] Set user page
- [ ] Set menu page
- [ ] Set table page

## Back End

- [ ] Set create user route

## Database

- [ ] menuItem, category, seats


裝機方法

1 。 裝NPM INSTALL 
2 。 去 folder [adminPage] --> [setCustomer] --> index.js 按第五十項的指示更改你的IP地址
     如果你是MAC 機，在終端機打 "ifconfig" --> en0 就有顯示
    en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether 0c:e4:41:ec:40:18 
	inet6 fe80::4b7:5847:d10f:6172%en0 prefixlen 64 secured scopeid 0xb 
	inet 192.168.1.22 (這才是你的IP地址)netmask 0xffffff00 broadcast 192.168.1.255（這過不是）
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active

3 。 去 folder [casher] --> [setCustomer] --> index.js 按第五十項的指示更改你的IP地址 
4 。 在你的PSQL 建立database 並用 sqlData_26Oct.sql 內的command line 建立data base 
5 。 在VS CODE 內 的ENV CONFIG ，輸入你的DATABASE 資料
6 。 用以下COMMAND，在你的DATABASE 中 建立你的首個USER 
     insert into staff_user (staff_name,password,staff_rank,status) values ('admin', '$2a$10$7xqTD0kWmopTqr/RytYh0e5ScwfIdeTxuvPWBYvDtN1axSdPdjzau', 'admin', true);
7。  在登入頁你的USERNAME 是 ADMIN ，密碼是：12345678