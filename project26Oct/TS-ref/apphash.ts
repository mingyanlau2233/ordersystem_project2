

import express from "express";
import * as bcrypt from 'bcryptjs';

const SALT_ROUNDS = 10;
////////////// hash promise
export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};
export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}
////////////

const app = express();

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`[info] listening to port ${PORT}`);
});

app.use(express.json())
//////
async function setpassword(password:string){

    const hash = await hashPassword(password) 
    
   
    return hash    
}
//////
app.get('/password',async (req ,res)=>{

    const  password =  await setpassword("123456")
    console.log("password:",password)
   console.log(await checkPassword('123456','$2a$10$Bmi7eNIFunlT3rcnI61MFeC/mwigOILoPwUvscvEWonLehDuv/YQC'))
    res.json(password)
})

 app.use(express.static("public"))