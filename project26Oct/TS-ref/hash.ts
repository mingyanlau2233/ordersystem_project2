import * as bcrypt from 'bcryptjs';

const SALT_ROUNDS = 10;

export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};


export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}



async function setpassword(password:string){

    const hash = await hashPassword(password) 

    return hash
     
}

console.log(setpassword("123456"))


async function check(){
let ans = await checkPassword("123456","$2a$10$TQOfcms1vnHpsplsdrCnZe5rYW9gr8OJFFE36qg5XYphrt3COqGyG")
console.log(ans)
}

check()