import express from "express";
import expressSession from 'express-session';
import * as bcrypt from 'bcryptjs';
const SALT_ROUNDS = 10;
export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};
export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}
import pg from 'pg';
import dotenv from 'dotenv';
//import {Client} from 'pg'; /// work with socket.io
//
dotenv.config();
const app = express();

//const io = new SocketIO(server);
///
///

app.use(expressSession({
  secret: 'restaurant',
  resave:true,
  saveUninitialized:true
}));
app.use(express.json())

export function setStaffUsers(){

  /////Create staff
  console.log(123)
  app.post('/adminPage/createStaffUsers',async(req,res)=>{
    console.log(234)
    let staffData =req.body
    console.log(staffData)
    let hash = await hashPassword(staffData.password)
    //console.log(hash)
    const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
     })
    await client.connect()
    await client.query(`INSERT INTO staff_user (staff_name, password, staff_rank, status)
    VALUES ($1, $2, $3, $4)`,[staffData.staffName, hash, staffData.staffRank , true] )
    

    await client.end()     

 res.json({ message: "success" })
}) 
///////review staffData
app.get('/adminPage/staffData',async (req ,res)=>{    

  const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
     })
    await client.connect()
    let staffData =await client.query(`select staff_id,staff_name, staff_rank,status from staff_user;`)
    await client.end() 
    //console.log(staffData.rows)
  res.json(staffData.rows)
})

app.post('/adminPage/renewStaffUsers',async(req,res)=>{   ///renew staffUser
      
  let staffData =req.body
 // console.log(staffData)
  const client = new pg.Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD
   })
  await client.connect()
  //console.log(staffData.StaffID)
  let checkStaff =await client.query(`SELECT * FROM staff_user where staff_id = '${staffData.StaffID}' `)
  //console.log(checkStaff.rows[0])

  if (checkStaff.rows[0] == undefined){
    
    await client.end() 
    res.status(202).end()
        return 
  }
  // 
  await client.query(`update staff_user Set staff_rank = '${staffData.staffRank}' , 
                      status = ${staffData.status} where staff_id =${staffData.StaffID};`)
  

 await client.end()     

res.json({ message: "success" })
}) 


app.use(express.static("adminPage"))
 }