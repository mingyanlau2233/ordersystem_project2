let socket;
window.onload = async () => {
  socket = io.connect();
  await checkNumberOfTable();
  await loadPayBillData();
  tableButton()
  loadloginStaff()
  socket.on("reloadpaybill", () => {
    //console.log(1)
    //checkNumberOfTable();
    loadPayBillData();
  });

  socket.on("tableOccupied", () => {
    console.log(2);
    //checkNumberOfTable();
    loadPayBillData();
  });
};

let numberOfTable;
let tableStatus;
async function checkNumberOfTable() {
  const resp = await fetch("/numberOfTable");
  const items = await resp.json();

  numberOfTable = items.rows.length;
  tableStatus = items.rows;
}

///////////
function tableButton() {
  console.log("tableButton")
  let tableButton = ``
  document.querySelector("#goTable").innerHTML = tableButton;
  for (let i = 1; i <= numberOfTable; i++) {
    tableButton = tableButton + `
      <div class="seats"><a class='billList' onclick='jumpTo(${i})'>${i}</a></div>
        `
  }
  document.querySelector("#goTable").innerHTML = tableButton;
}
///////////

function jumpTo(i){
  const billTop = document
  .querySelector(`#bill-${i}`)
  .getBoundingClientRect().top;
const y = document.querySelector(".fixed-top").getBoundingClientRect().bottom;
window.scrollTo({
  top: billTop - y,
  behavior: "smooth",
});

}

async function loadPayBillData() {
  await checkNumberOfTable()
  const resp = await fetch("/loadpaybilldata");
  const items = (await resp.json()).data;
  console.log(items);

  let payBillStr = ``;
  document.querySelector("#bills").innerHTML = payBillStr;

  for (let i = 1; i <= numberOfTable; i++) {
    console.log(i, tableStatus[i - 1].status);
    if (tableStatus[i - 1].status == true) {
      payBillStr += /*HTML*/ `
      <div class="bill-list" id='bill-${i}'>`;
    } else {
      payBillStr += /*HTML*/ `
      <div class="bill-list occupied" id='bill-${i}'>`;
    }
    payBillStr += /*HTML*/ `
    <div id='table-${i}'>
    <table class="table">
   <thead>
   <tr>
       <th class="tableCenter">Table ${i}</th>
       <th >Dish Name</th>
       <th  >Qty</th>
       <th >Sub-total </th>
   </tr>
   </thead>
   <tbody>
   `;
    //console.log(i)
    let sameTable = items.filter((table) => table.seats_id == i);
    //console.log(sameTable)

    let grandTotal = 0;
    for (let j of sameTable) {
      let total_price = j.quantity * j.price;
      grandTotal += total_price;
      payBillStr += /*HTML*/ `
              <tr>
                  <th ></th>
                  <td>${j.item_name}</td>
                  <td>${j.quantity}</td>
                  <td>$${total_price}</td>
              </tr>
              `;
    }

    payBillStr += /*HTML*/ `
            </tbody>
            <tfoot>
              <tr>
              <th scope="row">Total:</th>
              <td></td>
              <td></td>
              <td>$${grandTotal}</td>
              </tr>
            </tfoot>
            </table>
            </div>
            <div class="d-grid gap-2 col-6 mx-auto">
            <button type="button" class="btn btn-secondary"  onclick='printTable(${i})'>Print</button>
            <button type="button" class="btn btn-secondary" disabled='disabled' id="paid-${i}" onclick='paidBtnFnc(${i})'>Paid</button>
            </div>
            </div>
            `;

    document.querySelector("#bills").innerHTML = payBillStr;
  }
}

function printTable(i) {
  const printContents = document.querySelector(`#table-${i}`).innerHTML;
  const originalContents = document.body.innerHTML;

  document.body.innerHTML = printContents;
  window.print();
  document.body.innerHTML = originalContents;

  document.querySelector(`#paid-${i}`).removeAttribute("disabled");
  const billTop = document
    .querySelector(`#bill-${i}`)
    .getBoundingClientRect().top;
  const y = document.querySelector(".fixed-top").getBoundingClientRect().bottom;
  window.scrollTo({
    top: billTop - y,
    behavior: "smooth",
  });
}

function paidBtnFnc(i) {
  changeStatus(i);
  alertMsg(i);
}

async function changeStatus(i) {
  // console.log(formObject);
  let formObject = { i };
  const resp = await fetch("/changestatus", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(formObject),
  });

  if (resp.status == 200) {
    console.log("OK");
    // document.querySelector("body").innerHTML = "";
    loadPayBillData();
  }
}

function alertMsg(i) {
  swal(`Table ${i} is released`);
}

async function loadloginStaff() {

  const loginStaff = document.getElementById("loginStaff")

  const resp = await fetch('/adminPage/loadUser')

  const staff = (await resp.json())

  console.log(staff.staff)

  loginStaff.innerHTML = `Happy Coding Restaurant - Welcome back, ${staff.staff} !`

}

async function logOut() {

  const resp = await fetch('/logOut')
  if (resp.status === 200) {

    window.location = `/`
  }
}

/////////
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () { scrollFunction() };

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}