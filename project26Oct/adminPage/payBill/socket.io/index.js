let socket;

window.onload = async () => {
  socket = io.connect();

  await loadPayBillData();
  socket.on("confirmordercart", () => {
    console.log(1);
    loadPayBillData();
  });
};

async function loadPayBillData() {
  const resp = await fetch("/loadpaybilldata");
  const items = (await resp.json()).data;
  console.log(items);

  // let SeatsIdArr = [];
  // for (const i of items) {
  //   SeatsIdArr.push(i.seats_id);
  // }
  // let uniq = [...new Set(SeatsIdArr)];

  // let blockNum = [];
  // for (let i = 0; i < uniq.length; i++) {
  //   blockNum.push(String(""));
  // }
  // const orderMap = new Map()

  // console.log(blockNum);
  let payBillStr = ``;
  document.querySelector("body").innerHTML = payBillStr;

  for (let i = 0; i < items.length; i++) {
    // const item = items[i];
    console.log("seats_id", items[i].seats_id);
    if (items[i].order_info_status === "unpaid") {
      let total_price = items[i].quantity * items[i].price;
      if (i == 0) {
        payBillStr += /*HTML*/ `
          <table class="table">
          <thead>
          <tr>
              <th scope="col">SeatsID</th>
              <th scope="col">Dish Name</th>
              <th scope="col">Quantity</th>
              <th scope="col">Total $</th>
          </tr>
          </thead>
          <tbody>
          `;

        payBillStr += /*HTML*/ `
              <tr>
                  <th scope="row">${items[i].seats_id}</th>
                  <td>${items[i].item_name}</td>
                  <td>${items[i].quantity}</td>
                  <td>${total_price}</td>
              </tr>
              `;
      }

      if (i > 0) {
        payBillStr += /*HTML*/ `
        <tr>
            <th scope="row">${items[i].seats_id}</th>
            <td>${items[i].item_name}</td>
            <td>${items[i].quantity}</td>
            <td>${total_price}</td>
        </tr>
        `;
      }

      if (i < items.length - 1) {
        if (i > 0 && items[i].seats_id !== items[i + 1].seats_id) {
          payBillStr += /*HTML*/ `
            </tbody>
            </table>
            <button type="button" class="btn btn-secondary" id="Paid-${items[i].seats_id}" onclick='changeStatus(${items[i].seats_id})'>Paid</button>
            <br /><br />
            `;

          payBillStr += /*HTML*/ `
            <table class="table">
            <thead>
            <tr>
                <th scope="col">SeatsID</th>
                <th scope="col">Dish Name</th>
                <th scope="col">Quantity</th>
                <th scope="col">Total $</th>
            </tr>
            </thead>
            <tbody>
            `;
        }
      }

      if (i == items.length - 1) {
        payBillStr += /*HTML*/ `
        <tr>
            <th scope="row">${items[i].seats_id}</th>
            <td>${items[i].item_name}</td>
            <td>${items[i].quantity}</td>
            <td>${total_price}</td>
        </tr>
        `;

        payBillStr += /*HTML*/ `
            </tbody>
            </table>
            <button type="button" class="btn btn-secondary" id="Paid-${items[i].seats_id}" onclick='changeStatus(${items[i].seats_id})'>Paid</button>
            <br /><br />
            `;
      }
      document.querySelector("body").innerHTML = payBillStr;
    }
  }
}

async function changeStatus(seats_id) {
  let formObject = { seats_id };
  console.log(formObject);
  const resp = await fetch("/changestatus", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(formObject),
  });

  if (resp.status == 200) {
    console.log("OK");
    // document.querySelector("body").innerHTML = "";
    loadPayBillData();
  }
}
