//let socket;
let socket;

window.onload = async () => {
  //socket = io.connect('http://localhost:8080/');
  socket = io.connect();
  genQr();
  firstLoad();
  loadloginStaff();
  socket.on("changetablestatus", (msg) => {
    firstLoad();
    swal(msg.description);
  });

  //loadCurrentSeats()
};
///////
let table;
let seatsMayOccupied = 0;
let number = 0;
document
  .querySelector("#seatContainer")
  .addEventListener("click", function (event) {
    //console.log(event.target.innerHTML); // This is div.#child
    //console.log(event.target.className);

    if (event.target.className == "seatsEmpty") {
      //firstLoad()
      event.target.classList.remove("seatsEmpty");
      event.target.classList.add("seatsMayOccupied");
      seatsMayOccupied = event.target.innerHTML;
    } else if (event.target.className == "seatsMayOccupied") {
      event.target.classList.remove("seatsMayOccupied");
      event.target.classList.add("seatsEmpty");
      seatsMayOccupied = "";
    }
    console.log(seatsMayOccupied);
  });
//////// create Customer
async function getPassword() {
  const resp = await fetch("/password");
  let password = await resp.json();
  //console.log(password)
  return password;
}

async function generateQRCode() {
  let password = await getPassword();

  /////請於"<<<http://192.168.1.22:8080>>>"部份換上你的IP 地址/customer/"
  password = "http://192.168.1.22:8080/customer/" + password;
  if (password) {
    let qrcodeContainer = document.getElementById("qrcode");
    qrcodeContainer.innerHTML = "";
    new QRCode(qrcodeContainer, password);
    // document.getElementById("qrcode-container").style.display = "block";
    document.getElementById(
      "tableNum"
    ).innerHTML = `Table Number: ${table} <br> Number of Customers: ${number} `;
  }
}

function genQr() {
  const form = document.getElementById("qrCode");
  form.addEventListener("submit", async function (e) {
    e.preventDefault();
    // seatsMayOccupied.sort((a,b)=>a-b)
    if (seatsMayOccupied != 0) {
      let numberOfCustomers = form["numberOfCustomers"].value;
      let formObject = {
        seats: seatsMayOccupied,
        numberOfCustomers: numberOfCustomers,
      };
      table = seatsMayOccupied;
      number = numberOfCustomers;
      console.log(formObject);
      const resp = await fetch("/seats", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formObject),
      });
      seatsMayOccupied = generateQRCode();
      firstLoad();
    }
  });
}
////////
async function firstLoad() {
  const container = document.getElementById("seatContainer");

  const resp = await fetch("/seatsPlan");

  const seatsPlan = await resp.json();

  let current = seatsPlan.rows;
  current.sort((a, b) => a.seats_id - b.seats_id);
  let content = ``;
  for (let i of current) {
    if (i.status == true) {
      content += `
    <div class="seatsEmpty">${i.seats_id}</div> 
    `;
    } else {
      content += `
     <div class="seatsOccupied">${i.seats_id}</div> 
     `;
    }
  }

  container.innerHTML = content;
}

function print() {
  var print_div = document.getElementById("qrcode-container");
  var print_area = window.open();
  print_area.document.write(print_div.innerHTML);
  print_area.document.close();
  print_area.focus();
  print_area.print();
  print_area.close();
}

async function loadloginStaff() {
  const loginStaff = document.getElementById("loginStaff");

  const resp = await fetch("/adminPage/loadUser");

  const staff = await resp.json();

  console.log(staff.staff);

  loginStaff.innerHTML = `Happy Coding Restaurant - Welcome back, ${staff.staff} !`;
}

async function logOut() {
  const resp = await fetch("/logOut");
  if (resp.status === 200) {
    window.location = `/`;
  }
}
///
// async function loadCurrentSeats(){
//   socket.on("Occupied",(data=>{
//     const container = document.getElementById("seatContainer")
//     console.log(data)
//     data.sort((a,b)=> a.seats_id-b.seats_id)

//     let content = ``

//     for(let i of data){

//         if (i.status == true){
//         content += `
//         <div class="seatsEmpty">${i.seats_id}</div>
//         `
//          }else{ content += `
//          <div class="seatsOccupied">${i.seats_id}</div>
//          ` }

//     }
//     container.innerHTML=content
//   })
//   )
// }

async function logOut() {
  const resp = await fetch("/logOut");
  if (resp.status === 200) {
    window.location = `/`;
  }
}
