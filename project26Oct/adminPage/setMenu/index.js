window.onload = async () => {
    loadMenuData()
    // setStaffUsers()
    renewMenu()
    loadloginStaff()
}

function submitMsg() {
    alert("Item has been successfully created.")
}


async function loadloginStaff() {

    const loginStaff = document.getElementById("loginStaff")

    const resp = await fetch('/adminPage/loadUser')

    const staff = (await resp.json())

    console.log(staff.staff)

    loginStaff.innerHTML = `Happy Coding Restaurant - Welcome back, ${staff.staff} !`

}


async function loadMenuData() {

    const container = document.getElementById("Container")

    const resp = await fetch('/adminPage/menuData')

    const menuData = (await resp.json())

    menuData.sort((a, b) => a.cat_id - b.cat_id)
    console.log("after sort", menuData)
    let content = ``
    let y = 1
    for (let i of menuData) {

        content += `
        <tr>
    <th scope="row">${i.menuitem_id}</th>
    <td>${i.cat_id}</td>
    <td>${i.item_name}</td>
    <td>${i.hidden}</td>
    <td>
    <button class="btn btn-secondary" onclick="goToMenuItemDetailPage(${i.menuitem_id})"> Update </button>
    </td>
    <td><button class="btn btn-secondary" onclick="deleteMenuItem(${i.menuitem_id})"> Delete </button></td>
    </tr>
    `
        y++
    }
    container.innerHTML = content
}

async function goToMenuItemDetailPage(menuitem_id) {
    window.location = `/setMenu/update_menu.html?menuID=${menuitem_id}`

}

function renewMenu() {
    let menuId;

    document.querySelector('#Container').addEventListener('click', async function (event) {
        //console.log(event.target.innerHTML); // This is div.#child
        //console.log(event.target.id);
        let idNum = event.target.id
        menuData = `menuData${event.target.id}`
        //let fileId=`file${event.target.id}`
        //console.log("menu:",menuData)

        const form = document.getElementById(menuData)
        //let file = document.getElementById(fileId)
        form.addEventListener('submit', async function (e) {

            e.preventDefault()
            console.log('menuData:', form['image'])
            const formData = new FormData()
            formData.append('menuitem_id', form['itemID'].value,)
            formData.append('item_name', form['itemName'].value)
            formData.append('description', form['description'].value,)
            formData.append('price', form['price'].value,)
            formData.append('cat_id', form['catId'].value)
            formData.append('allergy', form['allergy'].value,)
            formData.append('quantity_limit', form['quantity'].value,)
            formData.append('hidden', form['hidden'].value,)
            formData.append('image', form['image'].files[0],)
            //  let formObject = {'menuitem_id':form['itemID'].value,
            //  'item_name':form['itemName'].value,'description':form['description'].value,
            //  'price':form['price'].value,'cat_id':form['catId'].value,'allergy':form['allergy'].value,
            //  'quantity_limit':form['quantity'].value,'hidden':form['hidden'].value,
            //   }

            console.log(formData)
            const resp = await fetch('/adminPage/renewMenu', {
                method: 'POST',

                body: formData,
                //     headers: {
                //       'Content-Type': 'application/json',
                //     },
                //     body: JSON.stringify(formObject)
            })

            if (resp.status == 200) {
                loadMenuData();
                document.getElementById('newItemCreate').innerHTML = "<br><span style='font-size: 20px'><i class='far fa-check-circle'></i> Menu information updated!</span>"
            }
        })





    })
}



async function logOut() {

    const resp = await fetch('/logOut')
    if (resp.status === 200) {

        window.location = `/`
    }
}

async function deleteMenuItem(menuitem_id) {
    let formObject = { menuitem_id };
    const resp = await fetch("/deleteMenu", {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(formObject),
    });

    if (resp.status == 200) {
        console.log("OK");
        // document.querySelector("body").innerHTML = "";
        loadMenuData();
        swal("Item has been deleted.");
    }
}

function onFileSelected(input) {
    console.log("onFileSelected")
    if (input.files && input.files[0]) {
        let reader = new FileReader()
        reader.onload = function (e) {
            document.querySelector("#menuPhoto").src = e.target.result
        }
        reader.readAsDataURL(input.files[0])
    }
}