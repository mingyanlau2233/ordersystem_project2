window.onload = async () => {
    await loadupdatemenu();
    await loadloginStaff();
    document.querySelector("#submit").addEventListener;
};
//
async function loadloginStaff() {

    const loginStaff = document.getElementById("loginStaff")

    const resp = await fetch('/adminPage/loadUser')

    const staff = (await resp.json())

    console.log(staff.staff)

    loginStaff.innerHTML = `Happy Coding Restaurant - Welcome back, ${staff.staff} !`

}

async function loadupdatemenu() {
    const params = new URLSearchParams(document.location.search.substring(1));
    const menuitem_id = params.get("menuID");
    // console.log(menuitem_id)

    const resp = await fetch(`/adminPage/singleMenuItem/${menuitem_id}`);
    // location.href = `menu_item.html?menu_item_id=${menuItemId}`
    const item = (await resp.json()).data[0];
    // console.log(item)
    console.log(item.cat_id);

    let HTMLStr = `
    <form action="/updateMenu" method='POST' enctype="multipart/form-data">
    <table class="table table-borderless" >
        <tbody >
            <tr>
                <th scope="row">Item ID:</th>
                <td>
                    <label>
                        <input type="text" name='itemID' readonly placeholder="Item ID" value='${item.menuitem_id}' required />
                    </label>
                </td>
            </tr>
            <tr>
                <th scope="row">Item Name:</th>
                <td>
                    <label>
                        <input type="text" name='itemName' placeholder="Item Name" value='${item.item_name}' required />
                    </label>
                </td>
            </tr>

            <tr>
                <th scope="row">Price:</th>
                <td>
                    <label>
                        <input type="number" name='price' value='${item.price}' required />
                    </label>
                </td>
            </tr>

            <tr>
                <th scope="row">CatagoryID:</th>
                <td>
                <label>
                <select name='catId' required>
                    <option value='1' ${createX(item.cat_id, 1)}>邪惡澱粉😈</option>
                    <option value='2' ${createX(item.cat_id, 2)}>健康蔬菜🥦</option>
                    <option value='3' ${createX(item.cat_id, 3)}>無肉不歡🍖</option>
                    <option value='4' ${createX(item.cat_id, 4)}>高杯暢飲🥃</option>
                    <option value='5' ${createX(item.cat_id, 5)}>幸福甜品🍮</option>
                    
            </label>
                </td>
            </tr>

            <tr>
                <th scope="row">Show/Hide:</th>
                <td>
                    <label>
                        <select name="hidden" id="hidden" value='${item.hidden}'>
                            <option value="true"  ${createX(item.hidden, true)} >Show</option>
                            <option value="false"  ${createX(item.hidden, false)}>Hide</option>
                    </label>
                </td>
            </tr>
            
            <tr>
                <th scope="row" value='${item.photo}'>Photo:</th>
                <td>
                    <label>
                        <input type='file' name="image" onchange="onFileSelected(this)" />
                    </label>
                </td>
            </tr>
            
        </tbody>
    </table>
    <img id="menuPhoto" src="${item.photo}" class="photo">
    <div class="d-grid gap-2">
    <input type='submit' class="btn btn-secondary" value="Confirm" id='submit'/>
    <input type='reset' class="btn btn-secondary" value="Restore" />
    <button type='reset' class="btn btn-secondary" onclick="window.location='../setMenu/index.html';">Back</button>
    </div>
</form>
    `
    document.querySelector('#itemDetails').innerHTML += HTMLStr
}

function onFileSelected(input) {
    console.log("onFileSelected")
    if (input.files && input.files[0]) {
        let reader = new FileReader()
        reader.onload = function (e) {
            document.querySelector("#menuPhoto").src = e.target.result
        }
        reader.readAsDataURL(input.files[0])
    }
}

function createX(inputId, value) {
    console.log(inputId, value)
    if (inputId == value) {
        return "selected"
    }
}