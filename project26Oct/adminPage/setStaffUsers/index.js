window.onload = async () => {
  loadStaffData()
  setStaffUsers()
  renewStaffUsers()
  loadloginStaff()
}


function setStaffUsers() {

  const form = document.getElementById('createStaff')
  form.addEventListener('submit', async function (e) {

    e.preventDefault()
    //console.log(form)
    let formObject = { 'staffName': form['staffName'].value, 'staffRank': form['staffRank'].value, 'password': form['password'].value }
    console.log(formObject)

    const resp = await fetch('/adminPage/createStaffUsers', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formObject)
    })
    if (resp.status === 200) {
      loadStaffData();
      document.getElementById('staffCreate').innerHTML =
        "<br><span style='font-size: 20px'><i class='far fa-check-circle'></i> New staff created!</span>"
    }
  })
}


async function loadStaffData() {

  const container = document.getElementById("staffDataContainer")

  const resp = await fetch('/adminPage/staffData')

  const staffData = (await resp.json())


  //console.log(staffData[0])
  let content = ``

  for (let i of staffData) {

    content += `
       <tr>
       <td>${i.staff_id}</td>
       <td>${i.staff_name}</td>
       <td>${i.staff_rank}</td>
       <td>${i.status}</td>
       </tr>
       `
  }


  container.innerHTML = content
}

function renewStaffUsers() {

  const form = document.getElementById('renewStaff')
  form.addEventListener('submit', async function (e) {

    e.preventDefault()
    //console.log(form)
    let formObject = { 'StaffID': form['staffId'].value, 'staffRank': form['staffRankR'].value, 'status': form['status'].value }
    console.log(formObject)

    const resp = await fetch('/adminPage/renewStaffUsers', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formObject)
    })
    if (resp.status == 202) {
      // console.log("wrong ID")
      document.getElementById('notExit').innerHTML = "<br><span style='font-size: 20px'><i class='far fa-times-circle'></i> This staff ID does not Exist.<br>Please try again.</span>"
      return;
    }

    if (resp.status == 200) {
      document.getElementById('notExit').innerHTML = "<br><span style='font-size: 20px'><i class='far fa-check-circle'></i> Staff information updated!</span>"
      loadStaffData()
    }
  }
  )
}
async function loadloginStaff(){
    
  const loginStaff = document.getElementById("loginStaff")
  
  const resp = await fetch('/adminPage/loadUser')

  const staff = (await resp.json())

  console.log(staff.staff)

  loginStaff.innerHTML = `Happy Coding Restaurant - Welcome back, ${staff.staff} !`

}

async function logOut() {

  const resp = await fetch('/logOut')
  if (resp.status === 200) {

    window.location = `/`
  }
}