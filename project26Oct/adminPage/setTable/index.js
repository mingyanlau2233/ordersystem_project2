window.onload = async () => {

    await loadCurrentSeatsPlan()
    setSeats()
    loadloginStaff()
  }


function setSeats() {

    const form = document.getElementById('set')
    form.addEventListener('submit', async function (e) {
  
      e.preventDefault()
     
      let formObject = { 'setSeats': form['seat'].value }
     // console.log(formObject)
      const resp = await fetch('/setSeats', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formObject)
      })
      if (resp.status===200){
        loadCurrentSeatsPlan()
        document.getElementById("feedBack").innerHTML = "<br><span style='font-size: 20px'><i class='far fa-check-circle'></i> Table information updated!</span>"
        
         }
    }
    )
  }

  async function loadCurrentSeatsPlan(){

    const container = document.getElementById("container")
    
    const resp = await fetch('/seatsPlan')
 
    const seatsPlan = (await resp.json())
     
    let num = seatsPlan.rows.length
    //console.log(num)
    let content = ``
 
    for(let i=1 ; i<=num ; i++){
 
        content += `
        <div class="seats">${i}</div> 
        `
    }
    
 
    container.innerHTML=content
  }

  async function loadloginStaff(){
    
    const loginStaff = document.getElementById("loginStaff")
    
    const resp = await fetch('/adminPage/loadUser')
  
    const staff = (await resp.json())
  
    console.log(staff.staff)
  
    loginStaff.innerHTML = `Happy Coding Restaurant - Welcome back, ${staff.staff} !`
  
  }

  async function logOut(){

    const resp = await fetch('/logOut')
    if (resp.status === 200) {  
   
        window.location = `/` 
       }
  }