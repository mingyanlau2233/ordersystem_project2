import express from "express";
import expressSession from "express-session"; /////
///////
import { Request, Response, NextFunction } from "express"; //check login
/////
import { v4 as uuidv4 } from "uuid"; ////gen keys
///////// api for connect server (crud)
import pg from "pg";
import dotenv from "dotenv";
//import {Client} from 'pg'; /// work with socket.io
//
dotenv.config();

///////// api for connect server (socketIO)
import http from "http";
import { Server as SocketIO } from "socket.io";

//// upload photo
import multer from "multer";
//import jsonfile from "jsonfile";
///////////////////  hash api and function
import * as bcrypt from "bcryptjs";
const SALT_ROUNDS = 10;
export async function hashPassword(plainPassword: string) {
  const hash = await bcrypt.hash(plainPassword, SALT_ROUNDS);
  return hash;
}
export async function checkPassword(
  plainPassword: string,
  hashPassword: string
) {
  const match = await bcrypt.compare(plainPassword, hashPassword);
  return match;
}
/////
//import { setStaffUsers } from "./setStaffUsers";

const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);
///

app.use(
  expressSession({
    secret: "restaurant",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(express.json());
///

const PORT = 8080;
server.listen(PORT, () => {
  console.log(`[info] listening to port ${PORT}`);
});
/////

//////////   Create New Customer
const client = new pg.Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
});

async function connectServer() {
  await client.connect();
}
connectServer();
//gen a customer name requested by font-end ---> store customer name / seats /   to SQL and return back to font-end
function createCustomer() {
  app.post("/seats", async (req, res) => {
    //console.log("seats:",req.body.seats)
    let seats = req.body.seats;

    req.session["seats"] = seats;
    req.session["numberOfCustomers"] = req.body.numberOfCustomers;

    await client.query(
      `update seats set status ='false' where seats_id ='${seats}'`
    );

    io.emit("tableOccupied");

    res.json({ success: true });
  });
  //////////////////
  app.get("/password", async (req, res) => {
    const password = uuidv4();
    // console.log("password:", password)

    let seatsNum = req.session["seats"];
    let numOfCustomer = req.session["numberOfCustomers"];
    //console.log(numOfCustomer , new Date())
    await client.query(
      `insert into session (qrcode,status,seats_id,num_of_customer,created_at) 
             values ($1, $2, $3, $4,$5)`,
      [password, true, seatsNum, numOfCustomer, new Date()]
    );

    //const renewSeats = await client.query('SELECT * from seats;');
    //console.log(renewSeats.rows)

    res.json(password);
  });
  //////////////////

  // customer after screen qr-code ---->1. if code is update , redirect to menu page
  app.get("/customer/:name", async (req, res) => {
    //const name = req.params.name;
    req.session["name"] = req.params.name;
    //console.log("sessionName:",req.session['name'])

    let customerData = await client.query(`select * from session`);
    //console.log(customerData.rows)

    for (let i of customerData.rows) {
      if (i.qrcode == req.session["name"] && i.status == true) {
        //console.log("customerData: ",i.id,i.name)
        req.session["qrcode"] = i.qrcode;
        //console.log(req.session['session_id'])
        //app.use(`/${i.qrcode}`, express.static("menu"));

        /////////
        res.redirect(`/menu.html`);

        return;
      }
    }

    res.end(`Wrong link or qr-code had expired, please contact our staff! `);
  });
}

// /////////////////////
createCustomer();

///////////////////// Set Seats

function setSeats() {
  app.post("/setSeats", async (req, res) => {
    let setSeats = Number(req.body.setSeats);
    //console.log(setSeats, typeof setSeats)

    await client.query(`TRUNCATE TABLE seats;`);
    for (let i = 1; i <= setSeats; i++) {
      await client.query(
        `insert into seats (seats_id,status) VALUES ('${i}','true')`
      );
    }
    await client.query(
      `update order_info Set order_info_status ='paid';`
    );
    res.json({ message: "success" });
  });

  /////
  app.get("/seatsPlan", async (req, res) => {
    let seatsPlan = await client.query(`select * from seats;`);

    res.json(seatsPlan);
  });
}
//////

setSeats();

/////// Create staff Users Pages
function setStaffUsers() {
  /////Create staff
  app.post("/adminPage/createStaffUsers", async (req, res) => {
    let staffData = req.body;
    // console.log(staffData)
    let hash = await hashPassword(staffData.password);
    //console.log(hash)
    let checkRepeatStaffName = await client.query(
      `select * from staff_user where staff_name = '${staffData.staffName}'`
    );

    if (checkRepeatStaffName.rows[0] == undefined) {
      await client.query(
        `INSERT INTO staff_user (staff_name, password, staff_rank, status)
    VALUES ($1, $2, $3, $4)`,
        [staffData.staffName, hash, staffData.staffRank, true]
      );
      res.status(200).end();
      return;
    }

    //res.status(201).end()

    res.json({ message: "success" });
  });
  ///////review staffData
  app.get("/adminPage/staffData", async (req, res) => {
    let staffData = await client.query(
      `select staff_id,staff_name, staff_rank,status from staff_user ORDER BY staff_id;`
    );

    //console.log(staffData.rows)
    res.json(staffData.rows);
  });

  app.post("/adminPage/renewStaffUsers", async (req, res) => {
    ///renew staffUser

    let staffData = req.body;
    // console.log(staffData)

    //console.log(staffData.StaffID)
    let checkStaff = await client.query(
      `SELECT * FROM staff_user where staff_id = '${staffData.StaffID}' `
    );
    //console.log(checkStaff.rows[0])

    if (checkStaff.rows[0] == undefined) {
      res.status(202).end();
      return;
    }
    //
    await client.query(`update staff_user Set staff_rank = '${staffData.staffRank}' , 
                      status = ${staffData.status} where staff_id =${staffData.StaffID};`);

    res.json({ message: "success" });
  });

  // app.use(express.static(path.join(__dirname, "./adminPage/setStaffUsers")))
}
setStaffUsers();
/////

function setMenu() {
  app.use(express.urlencoded({ extended: true }));
  let storage = multer.memoryStorage()
  const upload = multer({ storage: storage });

  app.post("/createMenu", upload.single("image"), async (req, res, next) => {
    //console.log(req.body)
    let data = req.body;
    //console.log(data)

    await client.query(
      `insert into menu_item (item_name,price,cat_id,photo,hidden) 
      values ($1, $2, $3, $4,$5)`,
      [
        data.itemName,
        data.price,
        data.catId,
        req.file == undefined ? null : "data:image/jpeg;base64," + req.file?.buffer.toString('base64'),
        data.hidden,
      ]
    );

    res.redirect("/setMenu");
  });

  app.post("/updateMenu", upload.single("image"), async (req, res, next) => {
    //console.log(req.body)
    let data = req.body;
    // console.log(data)
    if (req.file != undefined) {
      await client.query(`update  menu_item Set item_name = '${data.itemName}' , 
      price = ${data.price},cat_id = ${data.catId},
       hidden= ${data.hidden},
     photo = 'data:image/jpeg;base64,${req.file?.buffer.toString('base64')}'
     where menuitem_id =${data.itemID}`);
    } else {
      await client.query(`update  menu_item Set item_name = '${data.itemName}' , 
      price = ${data.price},cat_id = ${data.catId},
       hidden= ${data.hidden}
     where menuitem_id =${data.itemID}`);
    }


    res.redirect("/setMenu");
  });


  app.get("/adminPage/menuData", async (req, res) => {
    let menuData = await client.query(`SELECT * FROM menu_item ORDER BY cat_id, menuitem_id;`);
    //console.log(menuData.rows)
    res.json(menuData.rows);
  });



  app.get("/adminPage/singleMenuItem/:menuitem_id", async (req, res) => {
    const menuitem_id = req.params.menuitem_id;
    // console.log(menuitem_id);
    let menuData = await client.query(`select * from menu_item where menuitem_id = $1;`, [menuitem_id]);
    //console.log(menuData.rows)
    res.json({ data: menuData.rows });
    // // res.sendStatus(200)
    // let menuData = await client.query(
    //   `select * from menu_item where menuitem_id = $1;`,
    //   [menuitem_id]
    // );
    //console.log(menuData.rows)
    res.json(menuData.rows[0]);
  });



  // app.post("/adminPage/renewMenu", upload.single("image"), async (req, res) => {
  //   ///renew staffUser

  //   let menuData = req.body;
  //   //console.log("renewMenu",menuData)

  //   if (menuData.photoName != undefined) {
  //     await client.query(`update  menu_item Set item_name = '${menuData.item_name}' , 
  //      description = '${menuData.description}' , price = ${menuData.price},cat_id = ${menuData.cat_id},
  //      allergy = ${menuData.allergy} , quantity_limit=${menuData.quantity_limit}, hidden=${menuData.hidden},
  //      photo = '${menuData.photoName}'
  //      where menuitem_id =${menuData.menuitem_id};`);
  //   } else {
  //     await client.query(`update  menu_item Set item_name = '${menuData.item_name}' , 
  //      description = '${menuData.description}' , price = ${menuData.price},cat_id = ${menuData.cat_id},
  //      allergy = ${menuData.allergy} , quantity_limit=${menuData.quantity_limit}, hidden=${menuData.hidden}
  //      where menuitem_id =${menuData.menuitem_id};`);
  //   }

  //   res.redirect("/setMenu");
  // });

  app.delete("/deleteMenu", async (req, res) => {
    let menuId = req.body.menuitem_id;
    // console.log(menuId)
    await client.query(`DELETE FROM menu_item WHERE menuitem_id =${menuId};`);

    res.sendStatus(200);
  });
}
setMenu();

///////

function staffLogin() {
  app.post("/loginData", async (req, res, next) => {
    //console.log(req.body.staff_name)
    let staffData = await client.query(
      `SELECT * FROM staff_user where staff_name = '${req.body.staff_name}'`
    );
    //console.log(staffData.rows[0]) /// no data = undefined

    if (staffData.rows[0] == undefined) {
      //console.log("no this ID")
      res.status(201).end();
      return;
    }
    let check = await checkPassword(
      req.body.password,
      staffData.rows[0].password
    );
    //console.log(check)
    if (
      staffData.rows[0].staff_name == req.body.staff_name &&
      check == true &&
      staffData.rows[0].status == true
    ) {
      //console.log("all login data ok ")

      req.session["staff_rank"] = staffData.rows[0].staff_rank;
      req.session["staff_name"] = staffData.rows[0].staff_name;
      // console.log(
      //   "staff_rank :",
      //   req.session["staff_rank"],
      //   "staff_name",
      //   req.session["staff_name"]
      // );
      if (staffData.rows[0].staff_rank == "admin") {
        //console.log("admin OK")

        res.status(202).end();
        return;
      }
      if (staffData.rows[0].staff_rank == "cashier") {
        res.status(203).end();
        return;
      }
      if (staffData.rows[0].staff_rank == "waiter") {
        res.status(204).end();
        return;
      }
    }
    res.status(201).end();
    res.json({ message: "success" });
  });
}

staffLogin();

app.use(express.static("public"));
////

function menu() {
  // app.use((req, res, next) => {
  //   const cur = new Date().toISOString();
  //   console.log(`[INFO] ${cur} req path: ${req.path} method: ${req.method}`);
  //   next();
  // });

  app.get("/loadmenudata", async (req, res) => {
    const queryResult = await client.query(
      /*sql*/ `SELECT * FROM menu_item WHERE hidden = true ORDER BY menuitem_id`
    );

    res.json({ data: queryResult.rows });
  });

  app.post("/ordercart", async (req, res) => {
    // let session = req.session.XXXXXXX

    // const result = select * from order_cart where session_id = session AND menu_id = item_id
    //console.log("qrcode:",req.session['qrcode'])
    // console.log("req.body:",req.body);
    //////  get sessionData

    let sessionData = await client.query(
      /*SQL*/ `SELECT * from session  WHERE qrcode = $1`,
      [req.session["qrcode"]]
    );

    let session = sessionData.rows[0];
    //console.log("i am here ",sessionData.rows[0])
    //////
    let sessionID = sessionData.rows[0].session_id;
    req.session["session_id"] = sessionID;

    for (const i in req.body) {
      // console.log("i:",i);

      const result = await client.query(
        /*SQL*/ `SELECT * from order_cart WHERE session_id = $1 AND menu_id = $2`,
        [session.session_id, i]
      );
      // console.log(result.rows)
      if (result.rows[0] == undefined) {
        //console.log("nothing here")
        await client.query(
          /*SQL*/ `INSERT INTO order_cart (session_id,menu_id,quantity,status)VALUES ($1,$2,$3,$4)`,
          [sessionID, i, parseInt(req.body[i]), false]
        );
      } else {
        //console.log("somethingHere")
        await client.query(
          /*SQL*/ `UPDATE order_cart SET quantity = quantity + $1 WHERE menu_id = $2 And session_id = $3`,
          [parseInt(req.body[i]), i, sessionID]
        );
      }
    }

    // console.log(req.session['qrcode'])

    res.sendStatus(200);
    //.json({"qr":req.session['qrcode']})

    // res.json(result);
  });

  app.get("/loadmyordercartdata", async (req, res) => {
    // console.log("loadmyorder_qrcode:",req.session['session_id'])
    const queryResult =
      await client.query(/*sql*/ `SELECT item_name, quantity, price*quantity  AS total_price 
      FROM order_cart 
      LEFT JOIN menu_item
      ON menu_id = menuitem_id  where session_id = ${req.session["session_id"]}`);
    // console.log(queryResult.rows)
    res.json({ data: queryResult.rows });
  });

  app.post("/reset", async (req, res) => {
    let id = req.session["session_id"];
    //console.log(id)
    await client.query(`DELETE FROM order_cart WHERE session_id =${id};`);
    io.emit("updatedOrderCart");
    res.sendStatus(200);
  });

  app.post("/confirm", async (req, res) => {
    let sessionId = req.session["session_id"];
    let seatsId = await client.query(
      `select * from session where session_id = ${sessionId}`
    );
    seatsId = seatsId.rows[0]["seats_id"];
    // console.log("confirm: ","sessionId",sessionId,"seatsId",seatsId)

    let orderCartData = await client.query(
      `select * from order_cart where session_id = ${sessionId}`
    );

    if (orderCartData.rows[0] == undefined) {
      res.sendStatus(201);
      return;
    }

    let orderCart = orderCartData.rows;

    for (let i of orderCart) {
      // console.log(i,i.menu_id)
      let check =
        await client.query(`select * from order_info where session_id = ${sessionId}
        and menu_id = ${i.menu_id} `);

      if (check.rows[0] == undefined) {
        // console.log("nothing here")
        await client.query(
          /*SQL*/ `INSERT INTO order_info (seats_id,menu_id,quantity,order_info_status,ordered_at,session_id)VALUES ($1,$2,$3,$4,$5,$6)`,
          [seatsId, i.menu_id, i.quantity, "unpaid", new Date(), sessionId]
        );
        await client.query(
          /*SQL*/ `DELETE FROM order_cart WHERE session_id = ${sessionId} AND menu_id = ${i.menu_id}`
        );
      } else {
        //  console.log("somethingHere");
        await client.query(
          /*SQL*/ `UPDATE order_info SET quantity = quantity + $1 WHERE menu_id = $2 And session_id = $3`,
          [parseInt(i.quantity), i.menu_id, sessionId]
        );

        await client.query(
          /*SQL*/ `DELETE FROM order_cart WHERE session_id = ${sessionId} AND menu_id = ${i.menu_id}`
        );
      }
    }
    io.emit("updatedOrderInfo");
    io.emit("reloadpaybill");
    res.sendStatus(202);
  });

  app.get("/loadOrderInfo", async (req, res) => {
    //console.log("loadOrderINFO")
    // console.log("loadmyorder_qrcode:",req.session['session_id'])
    const queryResult =
      await client.query(/*sql*/ `SELECT item_name, quantity, price*quantity  AS total_price 
       FROM order_info 
       LEFT JOIN menu_item
       ON menu_id = menuitem_id  where session_id = ${req.session["session_id"]}`);
    // console.log(queryResult.rows)
    res.json({ data: queryResult.rows });
  });
}

menu();

function UserAndLogout() {
  app.get("/adminPage/loadUser", async (req, res) => {
    //console.log(req.session['staff_name'])
    res.json({ staff: req.session["staff_name"] });
  });
  app.get("/logOut", async (req, res) => {
    req.session["staff_name"] = "";
    req.session["staff_rank"] = "";
    //console.log("staffname:",req.session['staff_name'],"staffname:",req.session['staff_rank'])
    res.redirect("/");
  });
}

UserAndLogout();
//app.use(express.static("menu"))
////

function loadpaybilldata() {
  // app.use((req, res, next) => {
  //   const cur = new Date().toISOString();
  //   console.log(`[INFO] ${cur} req path: ${req.path} method: ${req.method}`);
  //   next();
  // });

  app.get("/loadmenudata", async (req, res) => {
    const queryResult = await client.query(
      /*sql*/ `SELECT * FROM menu_item ORDER BY menuitem_id`
    );
    res.json({ data: queryResult.rows });
  });

  app.post("/ordercart", async (req, res) => {
    // console.log(req.session);

    // console.log(req.body);
    for (const i in req.body) {
      //console.log(i);
      const result = await client.query(
        /*SQL*/ `SELECT * from order_cart WHERE menu_id = $1`,
        [i]
      );
      if (result.rowCount > 0) {
        await client.query(
          /*SQL*/ `UPDATE order_cart SET quantity = quantity + $1 WHERE menu_id = $2`,
          [parseInt(req.body[i]), i]
        );
      } else {
        await client.query(
          /*SQL*/ `INSERT INTO order_cart (menu_id,quantity)VALUES ($1,$2)`,
          [i, parseInt(req.body[i])]
        );
      }
    }
    io.emit("confirmordercart");
    res.sendStatus(200);
    // res.json(result);
  });

  app.get("/loadmyordercartdata", async (req, res) => {
    const queryResult =
      await client.query(/*sql*/ `SELECT item_name, quantity, price*quantity AS total_price 
      FROM order_cart 
      LEFT JOIN menu_item
      ON menu_id = menuitem_id`);
    res.json({ data: queryResult.rows });
  });

  app.get("/loadpaybilldata", async (req, res) => {
    // const queryResult = await client.query(/*sql*/ `SELECT * FROM order_info
    // LEFT JOIN menu_item
    // ON menu_id = menuitem_id`);

    const queryResult = await client.query(/*sql*/ `
    SELECT * 
    FROM order_info
    LEFT JOIN menu_item
    ON menu_id = menuitem_id
    WHERE order_info_status = 'unpaid'
    ORDER BY seats_id ASC
      `);

    // console.log(queryResult);
    res.json({ data: queryResult.rows });
  });

  app.post("/changestatus", async (req, res) => {
    //console.log(req.body);
    await client.query(
      /*sql*/ `UPDATE order_info SET order_info_status = $1 WHERE seats_id = $2`,
      ["paid", req.body.i]
    );

    await client.query(
      /*sql*/ `UPDATE session SET status = $1 WHERE seats_id = $2`,
      ["false", req.body.i]
    );

    await client.query(
      /*sql*/ `UPDATE seats SET status = $1 WHERE seats_id = $2`,
      ["true", req.body.i]
    );
    io.emit("changetablestatus", {
      description: `Table ${req.body.i} is released`,
    });

    const destination = "/logout.html";
    io.emit("logoutcustomer", destination);

    res.sendStatus(200);
  });

  app.get("/numberOfTable", async (req, res) => {
    // const queryResult = await client.query(/*sql*/ `SELECT * FROM order_info
    // LEFT JOIN menu_item
    // ON menu_id = menuitem_id`);

    const queryResult = await client.query(/*sql*/ `
    SELECT * 
    FROM seats
    ORDER BY seats_id
      `);

    //console.log(queryResult.rows.length);
    res.json(queryResult);
  });
}

loadpaybilldata();

app.use(express.static("menu"));
app.use(express.static("upload"));

const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
  if (
    req.session["staff_rank"] == "cashier" ||
    req.session["staff_rank"] == "admin"
  ) {
    next();
    return;
  }
  res.redirect("/");
};

app.use(isLoggedIn, express.static("casher"));

//
const isLoggedInForAdmin = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.session["staff_rank"] == "admin") {
    next();
    return;
  }
  res.redirect("/");
};

app.use(isLoggedInForAdmin, express.static("adminPage"));

// app.use(express.static(path.join(__dirname, "casher")));
// app.use(express.static("adminPage"));
