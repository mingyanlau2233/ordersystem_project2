CREATE TABLE "menu"(
    "id" SERIAL primary key,
    "description" TEXT ,
    "price" INTEGER ,
    "category" TEXT ,
    "photo" TEXT ,
    "allergy" BOOLEAN ,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE ,
    "quantity" INTEGER ,
    "sales" INTEGER 
);
ALTER TABLE
    "menu" ADD PRIMARY KEY("id");
CREATE TABLE "seats"(
    "id" SERIAL primary key,
    "status" BOOLEAN NOT NULL
);
ALTER TABLE
    "seats" ADD PRIMARY KEY("id");
CREATE TABLE "customer"(
    "id" SERIAL primary key,
    "name" TEXT ,
    "menu_id" INTEGER ,
    "seats_id" INTEGER ,
    "status" BOOLEAN 
);
ALTER TABLE
    "customer" ADD PRIMARY KEY("id");
CREATE TABLE "order_info"(
    "id" SERIAL primary key,
    "name" CHAR(255) ,
    "customer_id" INTEGER NULL,
    "meunu_id" INTEGER NOT NULL,
    "seats_id" INTEGER NOT NULL,
    "status" TEXT ,
    "order_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "finished_at" TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
    "total" INTEGER NOT NULL
);
ALTER TABLE
    "order_info" ADD PRIMARY KEY("id");
ALTER TABLE
    "order_info" ADD CONSTRAINT "order_info_customer_id_unique" UNIQUE("customer_id");
ALTER TABLE
    "order_info" ADD CONSTRAINT "order_info_meunu_id_unique" UNIQUE("meunu_id");
CREATE TABLE "staff_user"(
    "id" INTEGER NOT NULL,
    "staffname" TEXT NOT NULL,
    "password" INTEGER NOT NULL
);
ALTER TABLE
    "staff_user" ADD PRIMARY KEY("id");
ALTER TABLE
    "order_info" ADD CONSTRAINT "order_info_seats_id_foreign" FOREIGN KEY("seats_id") REFERENCES "seats"("id");
ALTER TABLE
    "order_info" ADD CONSTRAINT "order_info_customer_id_foreign" FOREIGN KEY("customer_id") REFERENCES "customer"("id");
ALTER TABLE
    "customer" ADD CONSTRAINT "customer_menu_id_foreign" FOREIGN KEY("menu_id") REFERENCES "menu"("id");
ALTER TABLE
    "customer" ADD CONSTRAINT "customer_seats_id_foreign" FOREIGN KEY("seats_id") REFERENCES "seats"("id");
ALTER TABLE
    "order_info" ADD CONSTRAINT "order_info_meunu_id_foreign" FOREIGN KEY("meunu_id") REFERENCES "menu"("id");