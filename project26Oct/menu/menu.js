let orderObj;
window.onload = async () => {
  orderObj = new Map();
  await loadMenuData();
};

async function loadMenuData() {
  const resp = await fetch("/loadmenudata");
  const items = (await resp.json()).data;

  let catArr = [``, ``, ``, ``,``];
  
  for (const item of items) {
    const itemName = item.item_name;
    const price = item.price;
    const photo = item.photo;
    const itemID = item.menuitem_id;
    //console.log(photo)
    catArr[item.cat_id - 1] += /*HTML*/ `
    <div class="col-12 col-md-6 col-lg-4 d-flex justify-content-center">
          <div class="menu-item card">
            <img
              src="${photo}"
              class="card-img-top photo"
              alt="${photo}"
            />
            <div class="card-body">
              <h5 class="card-title">${itemName}  $${price}</h5>
              <div class="d-flex justify-content-center align-items-center">
                <i class="fa-solid fa-circle-minus menu-item-button" id="${itemID}-minus" onclick="minusQty(${itemID})"></i>
                &nbsp; &nbsp;
                <input
                  id = "value-${itemID}"
                  class="form-control menu-item-quantity"
                  type="text"
                  value="0"
                  min="0"
                  disabled
                />
                &nbsp; &nbsp;
                <i class="fa-solid fa-circle-plus menu-item-button" id="${itemID}-plus" onclick="plusQty(${itemID})"></i>
              </div>
            </div>
          </div>
        </div>
    `;
  }

  const classArr = [".carbs", ".veges", ".meat", ".beverages",".desserts"];

  for (let i = 0; i < classArr.length; i++) {
    document.querySelector(classArr[i]).innerHTML = catArr[i];
  }
}

async function minusQty(itemID) {
  const qty = document.querySelector(`#value-${itemID}`);
  if (parseInt(qty.value) === 0) {
    qty.value = 0;
  } else {
    qty.value = parseInt(qty.value) - 1;
  }
  orderObj.set(itemID, parseInt(qty.value));
  console.log(orderObj);
}

async function plusQty(itemID) {
  const qty = document.querySelector(`#value-${itemID}`);
  qty.value = parseInt(qty.value) + 1;
  orderObj.set(itemID, parseInt(qty.value));
  console.log(orderObj);
}

document.querySelector(".cart").addEventListener("click", async () => {
  console.log(JSON.stringify(Object.fromEntries(orderObj)));
  const resp = await fetch("/ordercart", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(Object.fromEntries(orderObj)),
  });

  let resetArr = document.querySelectorAll(".menu-item-quantity");
  for (const i of resetArr) {
    i.value = 0;
  }
  
  if (resp.status === 200) {  
   
    window.location = `/order_cart.html` 
   }
});

document.querySelector(".reset").addEventListener("click",()=>{
  let resetArr = document.querySelectorAll(".menu-item-quantity");
  for (const i of resetArr) {
    i.value = 0;
  }
})

function onNavbarItemClicked() {
  document.querySelector("#navbarToggle").click()
}


//////

//Get the button
