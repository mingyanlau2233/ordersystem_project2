let socket;
window.onload = async () => {
  socket = io.connect();
  await loadMyOrderCartData();
  loadOrderInfo();

  socket.on("updatedOrderCart", () => {
    loadMyOrderCartData();
  });

  socket.on("updatedOrderInfo", () => {
    loadOrderInfo();
  });

  socket.on("logoutcustomer", () => {
    window.location = "/logout.html";
  });
};

async function loadMyOrderCartData() {
  document.querySelector("#myordercart").innerHTML = "";
  const resp = await fetch("/loadmyordercartdata");
  const items = (await resp.json()).data;

  let orderStr = ``;
  for (const i of items) {
    orderStr += /*HTML*/ `
      <tr>
      <td>${i.item_name}</td>
      <td>${i.quantity}</td>
      <td>${i.total_price}</td>
    </tr>
      `;
  }
  document.querySelector("#myordercart").innerHTML += orderStr;
}

document.querySelector("#reset").addEventListener("click", async () => {
  const resp = await fetch("/reset", {
    method: "POST",
  });
  if (resp.status === 200) {
    document.querySelector("#myordercart").innerHTML = "";
    loadMyOrderCartData();
  }
});

document.querySelector("#confirm").addEventListener("click", async () => {
  const resp = await fetch("/confirm", {
    method: "POST",
  });
  let feedback = document.getElementById("sumbitFeedback");
  if (resp.status === 201) {
    feedback.innerHTML = "nothing in ordercart";
    loadMyOrderCartData();
    loadOrderInfo();
  }
  if (resp.status === 202) {
    let time = new Date();
    let hour = time.getHours();
    let format;
    if (hour < 12) {
      if (hour < 10) {
        hour = 0 + hour;
      }
      format = "am";
    } else {
      hour -= 12;
      if (hour < 10 && hour > 1) {
        hour = "0" + hour;
      }
      if (hour == 0) {
        hour = "00";
      }
      format = "pm";
    }

    let minutes = time.getMinutes();
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    feedback.innerHTML = `Order confirmed on ${hour}:${minutes} ${format}`;
    loadMyOrderCartData();
    loadOrderInfo();
  }
});

async function loadOrderInfo() {
  const resp = await fetch("/loadOrderInfo");
  const items = (await resp.json()).data;

  let orderStr = ``;
  for (const i of items) {
    orderStr += /*HTML*/ `
      <tr>
      <td>${i.item_name}</td>
      <td>${i.quantity}</td>
      <td>${i.total_price}</td>
    </tr>
      `;
  }
  document.querySelector("#confirmedOrder").innerHTML = orderStr;
}
