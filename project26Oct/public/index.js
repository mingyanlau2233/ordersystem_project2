window.onload =()=>{

    loginForm()
}

function loginForm(){

   const form =document.getElementById('login')
   form.addEventListener('submit', async function(e){

       e.preventDefault()
       let formObject ={ 'staff_name':form['staffName'].value,'password':form['passWord'].value}
       console.log(formObject)
       const resp = await fetch('/loginData',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formObject)
       })
        if (resp.status === 201) {
           document.getElementById('errorInput').innerHTML=
           "<br><span style='font-size: 20px'><i class='fas fa-exclamation-triangle'></i> Wrong login information. <br>Please try again.</span>"
         }
         if (resp.status === 202) {  console.log("admin") 
             window.location = '/setMenu' }
         if (resp.status === 203) { window.location = '/cashier/setCustomer/' }
         if (resp.status === 204) { }
   })

}