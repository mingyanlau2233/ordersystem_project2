--! table name should be double qouted "" in query because of camel case
--! NOT NULL 暫時全部comment左先, 方便做testing

[menu_item] -> Bestseller menuitem<- [order_info] --many to many relationship


CREATE SEQUENCE serial
  start 1 increment 1;



CREATE TABLE "menu_item"(
    "menuitem_id" SERIAL PRIMARY KEY,
    "item_name" TEXT, -- NOT NULL,
    "description" TEXT,
    "price" INTEGER, -- NOT NULL,
    "cat_id" SMALLINT, -- NOT NULL,
    "photo" TEXT, -- NOT NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE, -- NOT NULL,
    "hidden" BOOLEAN
);

CREATE TABLE "order_info"(   
    "orderinfo_id" SERIAL PRIMARY KEY,
    "seats_id" INTEGER, -- NOT NULL,
    "menu_id" INTEGER, 
    "order_info_status" TEXT, -- NOT NULL, pending, processing, finished
    "ordered_at" TIMESTAMP(0) WITHOUT TIME ZONE, -- NOT NULL,
    "finished_at" TIMESTAMP(0) WITHOUT TIME ZONE, -- NOT NULL
    "quantity" SMALLINT,
    "session_id" INTEGER
);

CREATE TABLE "category"(
    "cat_id" SERIAL PRIMARY KEY,
    "type" TEXT
);

CREATE TABLE "session"( 
    "session_id" SERIAL PRIMARY KEY,
    "qrcode" TEXT, -- NOT NULL,
    "seats_id" TEXT, -- NOT NULL,
    "status" BOOLEAN, -- NOT NULL, False = not paid, True = paid
    "num_of_customer" SMALLINT, -- NOT NULL,
    "ordered_items" INTEGER, 
    "total$" INTEGER, 
    "created_at" TIMESTAMP, -- NOT NULL,
    "closed_at" TIMESTAMP -- NOT NULL
);

CREATE TABLE "order_cart"(    
    "ordercart_id" SERIAL PRIMARY KEY,
    "session_id" INTEGER,
    "menu_id" SMALLINT, -- NOT NULL,
    "quantity" SMALLINT, -- NOT NULL
    "status" BOOLEAN 
);

CREATE TABLE "seats"(
    "seats_id" SMALLINT,
    "status" BOOLEAN -- NOT NULL
);

CREATE TABLE "staff_user"( 
    "staff_id" SERIAL PRIMARY KEY,
    "staff_name" TEXT, -- NOT NULL,
    "password" TEXT, -- NOT NULL,
    "staff_rank" TEXT, -- NOT NULL,
    "status" BOOLEAN -- NOT NULL
    );


